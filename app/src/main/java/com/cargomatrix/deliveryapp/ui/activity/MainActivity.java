package com.cargomatrix.deliveryapp.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.View;

import com.cargomatrix.deliveryapp.DeliveryApplication;
import com.cargomatrix.deliveryapp.R;
import com.cargomatrix.deliveryapp.databinding.ActivityMainBinding;
import com.cargomatrix.deliveryapp.model.data.Location;
import com.cargomatrix.deliveryapp.model.db.LocationRepository;
import com.cargomatrix.deliveryapp.view_model.MainActivityView;
import com.cargomatrix.deliveryapp.view_model.MainViewModel;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements MainActivityView {

    @Inject
    public LocationRepository mLocationRepository;

    private static final int RC_HANDLE_CAMERA_PERM = 2;
    public static String LOCATION_ID = "location_id";
    private CameraSource mCameraSource;
    private List<Location> mLocations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DeliveryApplication.getComponent().inject(this);

        mViewModel = new MainViewModel(mLocationRepository);

        mViewModel.attach(this);

        bindView(R.layout.activity_main);
        mBinding.setDoneHandler(new DoneHandler());
        initScanner();

        if (getIntent().getIntExtra(LOCATION_ID, -1) != -1) {
            mViewModel.removeLocation(getIntent().getIntExtra(LOCATION_ID, -1));
        }else{
            mViewModel.insertItems();
            mViewModel.loadLocations();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
        }
    }

    @Override
    public void loadLocations(List<Location> locations) {
        mLocations = locations;
    }

    private void initScanner() {
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        mCameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(800, 600)
                .setAutoFocusEnabled(true)
                .build();

        mBinding.cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        mCameraSource.start(mBinding.cameraView.getHolder());
                    } else {
                        requestCameraPermission();
                    }
                } catch (IOException ignore) {
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                mCameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() != 0) {
                    mBinding.barcodeInfo.post(() -> {
                                String barCodeInput = String.valueOf((barcodes.valueAt(0).displayValue));
                                if (!barCodeInput.isEmpty() && barCodeInput.length() > 4 && barCodeInput.startsWith("QD")) {
                                    mBinding.barcodeInfo.setText(barCodeInput);
                                    mBinding.barcodeInfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
                                } else {
                                    mBinding.barcodeInfo.setText(getString(R.string.invalid_bar_code_format));
                                    mBinding.barcodeInfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.errorColor));
                                }
                            }
                    );
                }
            }
        });
    }

    private void requestCameraPermission() {
        final String[] permissions = new String[]{Manifest.permission.CAMERA};
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }
        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                // we have permission, so create the camera source
                mCameraSource.start(mBinding.cameraView.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public class DoneHandler implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            String barCodeInput = mBinding.barcodeInfo.getText().toString().trim();
            if (!barCodeInput.isEmpty() && barCodeInput.length() > 4 && barCodeInput.startsWith("QD") && mLocations!=null) {
                for (Location location : mLocations) {
                    if (location.getBarCode().equals(barCodeInput)) {
                        mBinding.barcodeInfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
                        Intent intent = new Intent(getApplicationContext(), SpecimenActivity.class);
                        intent.putExtra(LOCATION_ID, location.getId());
                        startActivity(intent);
                        mBinding.barcodeInfo.setText("");
                        return;
                    }
                }
                mBinding.barcodeInfo.setText(getString(R.string.invalid_location));
                mBinding.barcodeInfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.errorColor));
            } else if (barCodeInput.isEmpty()) {
                mBinding.barcodeInfo.setText(getString(R.string.scan_info));
                mBinding.barcodeInfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.errorColor));
            } else {
                mBinding.barcodeInfo.setText(getString(R.string.invalid_bar_code_format));
                mBinding.barcodeInfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.errorColor));
            }
        }
    }
}
