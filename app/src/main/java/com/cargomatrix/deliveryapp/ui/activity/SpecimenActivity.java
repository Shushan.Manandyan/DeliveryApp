package com.cargomatrix.deliveryapp.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.View;

import com.cargomatrix.deliveryapp.DeliveryApplication;
import com.cargomatrix.deliveryapp.R;
import com.cargomatrix.deliveryapp.databinding.ActivitySpecimenBinding;
import com.cargomatrix.deliveryapp.model.data.Specimen;
import com.cargomatrix.deliveryapp.model.db.SpecimenRepository;
import com.cargomatrix.deliveryapp.ui.adapter.SpecimenAdapter;
import com.cargomatrix.deliveryapp.view_model.SpecimenActivityView;
import com.cargomatrix.deliveryapp.view_model.SpecimenViewModel;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import static com.cargomatrix.deliveryapp.ui.activity.MainActivity.LOCATION_ID;

public class SpecimenActivity extends BaseActivity<ActivitySpecimenBinding, SpecimenViewModel> implements SpecimenActivityView {

    @Inject
    public SpecimenRepository mSpecimenRepository;

    private SpecimenAdapter mSpecimenAdapter;
    private List<Specimen> mSpecimenList;
    private CameraSource mCameraSource;
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    public class Handler {
        public boolean onNavigationClick(@NonNull MenuItem item) {
            switch (item.getItemId()) {

                case R.id.navigation_home:
                    mSpecimenAdapter.setDropEnable(false);
                    mBinding.setDropEnable(false);
                    return true;

                case R.id.navigation_notifications:
                    mSpecimenAdapter.setDropEnable(true);
                    mBinding.setDropEnable(true);
                    return true;

                default:
                    return false;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DeliveryApplication.getComponent().inject(this);

        mViewModel = new SpecimenViewModel(mSpecimenRepository);
        mViewModel.attach(this);

        bindView(R.layout.activity_specimen);
        Handler handler = new Handler();
        mBinding.setNavigationHandler(handler);
        mBinding.setSpecimenHandler(new DoneHandler());

        initScanner();

        mSpecimenAdapter = new SpecimenAdapter();
        mSpecimenAdapter.setSpecimenListener(this);
        mBinding.specimens.setAdapter(mSpecimenAdapter);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestCameraPermission();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mViewModel.insertItems();
        mViewModel.getSpecimenListByLocation(getIntent().getIntExtra(LOCATION_ID, -1));
    }


    @Override
    public void onDrop(Specimen specimen) {
        specimen.setPickedUp(false);
        specimen.setSearchLocationId(getIntent().getIntExtra(LOCATION_ID, -1));
        mViewModel.updateSpecimen(specimen, getIntent().getIntExtra(LOCATION_ID, -1));
    }

    @Override
    public void onPick(String barCode) {
        if (mSpecimenList != null) {
            for (Specimen specimen : mSpecimenList) {
                if(specimen.getBarCode().equals(barCode)){
                    specimen.setPickedUp(true);
                    mViewModel.updateSpecimen(specimen,getIntent().getIntExtra(LOCATION_ID, -1));
                }
            }
        }
    }

    @Override
    public void onCloseLocation() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(LOCATION_ID, getIntent().getIntExtra(LOCATION_ID, -1));
        startActivity(intent);
    }

    @Override
    public void loadSpecimens(List<Specimen> specimens) {
        mSpecimenList = specimens;
    }

    @Override
    public void loadPickedUpSpecimens(List<Specimen> specimens) {
        mSpecimenAdapter.setSpecimenData(specimens);
    }
    private void initScanner() {
        BarcodeDetector barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        mCameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(800, 600)
                .setAutoFocusEnabled(true)
                .build();

        mBinding.cameraView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(SpecimenActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        mCameraSource.start(mBinding.cameraView.getHolder());
                    } else {
                        requestCameraPermission();
                    }
                } catch (IOException ignore) {
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                mCameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() != 0) {
                    mBinding.barcodeInfo.post(() -> mBinding.barcodeInfo.setText(    // Update the TextView
                            String.valueOf((barcodes.valueAt(0).displayValue))
                    ));
                }
            }
        });
    }

    private void requestCameraPermission() {
        final String[] permissions = new String[]{Manifest.permission.CAMERA};
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }
        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                // we have permission, so create the camera source
                mCameraSource.start(mBinding.cameraView.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public class DoneHandler implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()){

                case R.id.closeLocation:
                    onCloseLocation();
                    break;

                case R.id.pickUpSpecimen:
                    String barCodeInput = mBinding.barcodeInfo.getText().toString().trim();
                    if (!barCodeInput.isEmpty() && barCodeInput.length() > 4 && barCodeInput.startsWith("QD")) {
                        onPick(barCodeInput);
                    } else if (barCodeInput.isEmpty()) {
                        mBinding.barcodeInfo.setText(getString(R.string.scan_specimen_info));
                        mBinding.barcodeInfo.setTextColor(ContextCompat.getColor(SpecimenActivity.this, R.color.errorColor));
                    } else {
                        mBinding.barcodeInfo.setText(getString(R.string.invalid_bar_code_format));
                        mBinding.barcodeInfo.setTextColor(ContextCompat.getColor(SpecimenActivity.this, R.color.errorColor));
                    }
                    break;

            }

        }
    }
}