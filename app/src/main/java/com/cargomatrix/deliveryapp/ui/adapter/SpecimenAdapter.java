package com.cargomatrix.deliveryapp.ui.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.cargomatrix.deliveryapp.R;
import com.cargomatrix.deliveryapp.databinding.SpecimenRowLayoutBinding;
import com.cargomatrix.deliveryapp.model.data.Specimen;
import com.cargomatrix.deliveryapp.view_model.SpecimenListener;

import java.util.ArrayList;
import java.util.List;

public class SpecimenAdapter extends RecyclerView.Adapter<SpecimenAdapter.SpecimenViewHolder> {

    private List<Specimen> mSpecimenData = new ArrayList<>();
    private boolean mDropEnable;
    private SpecimenRowLayoutBinding mBinding;
    private SpecimenListener mSpecimenListener;

    @Override
    public SpecimenViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mBinding = DataBindingUtil.inflate(inflater, R.layout.specimen_row_layout, parent, false);
        return new SpecimenViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(SpecimenViewHolder holder, int position) {
        holder.update(mSpecimenData.get(position));
        holder.setListener();
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mSpecimenData.size();
    }


    public void setSpecimenData(List<Specimen> specimenData) {
        mSpecimenData = specimenData;
        notifyDataSetChanged();
    }

    public void setSpecimenListener(SpecimenListener specimenListener) {
        mSpecimenListener = specimenListener;
    }

    public void setDropEnable(boolean dropEnable) {
        mDropEnable = dropEnable;
        notifyDataSetChanged();
    }

    public class SpecimenViewHolder extends RecyclerView.ViewHolder {

        SpecimenRowLayoutBinding binding;

        public SpecimenViewHolder(SpecimenRowLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void update(Specimen specimen) {
            binding.setSpecimen(specimen);
            binding.setDropEnable(mDropEnable);
            binding.executePendingBindings();
        }

        public void setListener() {
            binding.dropSpecimen.setOnClickListener(view -> {
                if (mSpecimenListener != null && (Integer) itemView.getTag() >= 0) {
                    mSpecimenListener.onDrop(mSpecimenData.get((Integer) itemView.getTag()));
                }
            });
        }
    }
}
