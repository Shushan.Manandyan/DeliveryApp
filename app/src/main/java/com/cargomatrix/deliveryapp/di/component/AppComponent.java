package com.cargomatrix.deliveryapp.di.component;

import android.app.Application;

import com.cargomatrix.deliveryapp.di.module.RoomModule;
import com.cargomatrix.deliveryapp.model.db.DeliveryDatabase;
import com.cargomatrix.deliveryapp.model.db.LocationDao;
import com.cargomatrix.deliveryapp.model.db.LocationRepository;
import com.cargomatrix.deliveryapp.model.db.SpecimenDao;
import com.cargomatrix.deliveryapp.model.db.SpecimenRepository;
import com.cargomatrix.deliveryapp.ui.activity.MainActivity;
import com.cargomatrix.deliveryapp.di.module.AppModule;
import com.cargomatrix.deliveryapp.ui.activity.SpecimenActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, RoomModule.class})
public interface AppComponent {

    Application application();

    DeliveryDatabase appDatabase();

    LocationDao locationDao();

    SpecimenDao specimenDao();

    LocationRepository locationRepository();

    SpecimenRepository specimenRepository();

    void inject(MainActivity activity);

    void inject(SpecimenActivity activity);
}
