package com.cargomatrix.deliveryapp.di.module;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.cargomatrix.deliveryapp.model.db.DeliveryDatabase;
import com.cargomatrix.deliveryapp.model.db.LocationDao;
import com.cargomatrix.deliveryapp.model.db.LocationDataSource;
import com.cargomatrix.deliveryapp.model.db.LocationRepository;
import com.cargomatrix.deliveryapp.model.db.SpecimenDao;
import com.cargomatrix.deliveryapp.model.db.SpecimenDataSource;
import com.cargomatrix.deliveryapp.model.db.SpecimenRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    private DeliveryDatabase mDeliveryDatabase;

    public RoomModule(Application application) {
        mDeliveryDatabase = Room.databaseBuilder(application, DeliveryDatabase.class, "delivery.db").allowMainThreadQueries().build();
    }

    @Singleton
    @Provides
    DeliveryDatabase providesRoomDatabase() {
        return mDeliveryDatabase;
    }

    @Singleton
    @Provides
    LocationDao providesLocationDao(DeliveryDatabase deliveryDatabase) {
        return deliveryDatabase.getLocationDao();
    }

    @Singleton
    @Provides
    SpecimenDao providesSpecimenDao(DeliveryDatabase deliveryDatabase) {
        return deliveryDatabase.getSpecimenDao();
    }

    @Singleton
    @Provides
    LocationRepository locationRepository(LocationDao locationDao) {
        return new LocationDataSource(locationDao);
    }

    @Singleton
    @Provides
    SpecimenRepository specimenRepository(SpecimenDao specimenDao) {
        return new SpecimenDataSource(specimenDao);
    }
}
