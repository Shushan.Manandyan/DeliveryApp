package com.cargomatrix.deliveryapp.view_model;

import io.reactivex.disposables.CompositeDisposable;

public class BaseViewModel<T extends IView> {

    protected CompositeDisposable mCompositeDisposable;

    T mView;

    public BaseViewModel() {
        mCompositeDisposable = new CompositeDisposable();
    }

    public void attach(T view) {
        mView = view;
    }

    public void detach() {
        mView = null;
    }

    public void clearSubscription() {
        mCompositeDisposable.clear();
    }
}
