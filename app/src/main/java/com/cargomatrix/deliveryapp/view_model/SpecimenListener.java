package com.cargomatrix.deliveryapp.view_model;

import com.cargomatrix.deliveryapp.model.data.Specimen;

public interface SpecimenListener {
    void onDrop(Specimen specimen);

    void onPick(String barCode);

    void onCloseLocation();
}
