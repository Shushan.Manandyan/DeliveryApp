package com.cargomatrix.deliveryapp.view_model;

import com.cargomatrix.deliveryapp.model.data.Specimen;
import com.cargomatrix.deliveryapp.model.db.SpecimenRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SpecimenViewModel extends BaseViewModel<SpecimenActivityView> {

    private SpecimenRepository mSpecimenRepository;

    public SpecimenViewModel(SpecimenRepository specimenRepository) {
        mSpecimenRepository = specimenRepository;
    }

    public void insertItems() {
        mCompositeDisposable.add(mSpecimenRepository.countItems()
                .subscribeOn(Schedulers.computation())
                .subscribe(locations -> {
                            if (locations <= 0) {
                                    Specimen specimen1 = new Specimen();
                                    specimen1.setBarCode("QD234");
                                    specimen1.setSearchLocationId(1);
                                    specimen1.setName("Specimen1");

                                    Specimen specimen2 = new Specimen();
                                    specimen2.setBarCode("QD235");
                                    specimen2.setSearchLocationId(1);
                                    specimen2.setName("Specimen2");

                                    Specimen specimen3 = new Specimen();
                                    specimen3.setBarCode("QD236");
                                    specimen3.setSearchLocationId(2);
                                    specimen3.setName("Specimen3");

                                    Specimen specimen4 = new Specimen();
                                    specimen4.setBarCode("QD237");
                                    specimen4.setSearchLocationId(2);
                                    specimen4.setName("Specimen4");

                                    Specimen specimen5 = new Specimen();
                                    specimen5.setBarCode("QD238");
                                    specimen5.setSearchLocationId(2);
                                    specimen5.setName("Specimen5");

                                    Specimen specimen6 = new Specimen();
                                    specimen6.setBarCode("QD239");
                                    specimen6.setSearchLocationId(2);
                                    specimen6.setName("Specimen6");

                                    mSpecimenRepository.insertAll(specimen1, specimen2, specimen3, specimen4,specimen5, specimen6);
                            }
                        },
                        throwable -> mView.error(new Throwable())
                ));
    }

    public void getSpecimenListByLocation(int locationId) {
        mCompositeDisposable.add(mSpecimenRepository.findByLocation(locationId, false)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(specimens -> {
                            if (specimens != null) {
                                mView.loadSpecimens(specimens);
                            }
                            getPickedUpSpecimens();
                        },
                        throwable -> mView.error(new Throwable())));

    }

    public void getPickedUpSpecimens() {
        mCompositeDisposable.add(mSpecimenRepository.getPickedUpSpecimens()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(specimens -> {
                            if (specimens != null) {
                                mView.loadPickedUpSpecimens(specimens);
                            }
                        },
                        throwable -> mView.error(new Throwable())));
    }

    public void updateSpecimen(Specimen specimen, int locationId) {
        mCompositeDisposable.add(mSpecimenRepository.update(specimen)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(specimens -> {
                            getSpecimenListByLocation(locationId);
                        },
                        throwable -> mView.error(new Throwable())));

    }
}

