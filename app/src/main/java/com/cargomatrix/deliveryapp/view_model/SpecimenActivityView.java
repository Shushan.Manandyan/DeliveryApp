package com.cargomatrix.deliveryapp.view_model;

import com.cargomatrix.deliveryapp.model.data.Specimen;

import java.util.List;

public interface SpecimenActivityView extends IView, SpecimenListener {

    void loadSpecimens(List<Specimen> specimens);

    void loadPickedUpSpecimens(List<Specimen> specimens);

    @Override
    void onDrop(Specimen specimen);

    @Override
    void onPick(String barCode);
}
