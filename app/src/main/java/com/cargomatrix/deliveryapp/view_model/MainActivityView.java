package com.cargomatrix.deliveryapp.view_model;

import com.cargomatrix.deliveryapp.model.data.Location;

import java.util.List;

public interface MainActivityView extends IView {

    void loadLocations(List<Location> items);

}
