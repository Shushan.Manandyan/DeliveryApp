package com.cargomatrix.deliveryapp.view_model;

import com.cargomatrix.deliveryapp.model.data.Location;
import com.cargomatrix.deliveryapp.model.db.LocationRepository;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends BaseViewModel<MainActivityView> {

    private LocationRepository mLocationRepository;

    public MainViewModel(LocationRepository locationRepository) {
        mLocationRepository = locationRepository;
    }

    public void insertItems() {
        mCompositeDisposable.add(mLocationRepository.countLocations()
                .subscribeOn(Schedulers.computation())
                .subscribe(locations -> {
                            if (locations <= 0) {
                                Location location1 = new Location();
                                location1.setBarCode("QD123");
                                Location location2 = new Location();
                                location2.setBarCode("QD124");
                                Location location3 = new Location();
                                location3.setBarCode("QD125");
                                Location location4 = new Location();
                                location4.setBarCode("QD126");
                                Location location5 = new Location();
                                location5.setBarCode("QD127");
                                Location location6 = new Location();
                                location6.setBarCode("QD128");
                                mLocationRepository.insertAll(location1, location2, location3, location4, location5, location6);
                            }
                        },
                        throwable -> mView.error(new Throwable())
                ));
    }

    public void loadLocations() {
        mCompositeDisposable.add(mLocationRepository.getAll()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(locations -> {
                            if (locations != null) {
                                mView.loadLocations(locations);
                            }
                        },
                        throwable -> mView.error(new Throwable())));

    }

    public void removeLocation(int locationId){
        mCompositeDisposable.add(mLocationRepository.delete(locationId)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(locations -> {
                          loadLocations();
                        },
                        throwable -> mView.error(new Throwable())));

        loadLocations();
    }
}

