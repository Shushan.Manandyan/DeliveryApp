package com.cargomatrix.deliveryapp.model.db;

import com.cargomatrix.deliveryapp.model.data.Location;
import com.cargomatrix.deliveryapp.model.data.Specimen;

import org.reactivestreams.Subscriber;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class LocationDataSource implements LocationRepository {

    private LocationDao mLocationDao;

    @Inject
    public LocationDataSource(LocationDao locationDao) {
        mLocationDao = locationDao;
    }

    @Override
    public Flowable<List<Location>> getAll() {
        return mLocationDao.getAll();
    }

    @Override
    public void insertAll(Location... locations) {
        mLocationDao.insertAll(locations);
    }

    @Override
    public Flowable<Integer> countLocations() {
        return mLocationDao.countLocations();
    }

    @Override
    public Flowable<Boolean> delete(int locationId) {
        mLocationDao.delete(locationId);

        return new Flowable<Boolean>() {
            @Override
            protected void subscribeActual(Subscriber<? super Boolean> s) {
            }

        };
    }
}
