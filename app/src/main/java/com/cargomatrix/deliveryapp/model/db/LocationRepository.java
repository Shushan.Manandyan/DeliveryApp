package com.cargomatrix.deliveryapp.model.db;

import com.cargomatrix.deliveryapp.model.data.Location;

import java.util.List;

import io.reactivex.Flowable;

public interface LocationRepository {

    Flowable<List<Location>> getAll();

    void insertAll(Location... locations);

    Flowable<Integer> countLocations();

    Flowable<Boolean> delete(int locationId);
}
