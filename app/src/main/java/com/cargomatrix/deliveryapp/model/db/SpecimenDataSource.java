package com.cargomatrix.deliveryapp.model.db;

import com.cargomatrix.deliveryapp.model.data.Specimen;

import org.reactivestreams.Subscriber;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class SpecimenDataSource implements SpecimenRepository {

    private SpecimenDao mSpecimenDao;

    @Inject
    public SpecimenDataSource(SpecimenDao specimenDao) {
        mSpecimenDao = specimenDao;
    }

    @Override
    public Flowable<List<Specimen>> getPickedUpSpecimens() {
        return mSpecimenDao.getPickedUpSpecimens(true);
    }

    @Override
    public Flowable<Integer> countItems() {
        return mSpecimenDao.countItems();
    }

    @Override
    public Flowable<List<Specimen>> findByLocation(int locationId, boolean pickedUp) {
        return mSpecimenDao.findByLocation(locationId, pickedUp);
    }

    @Override
    public void insertAll(Specimen... specimens) {
        mSpecimenDao.insertAll(specimens);
    }

    @Override
    public Flowable<Boolean> update(Specimen specimens) {
        return new Flowable<Boolean>() {
            @Override
            protected void subscribeActual(Subscriber<? super Boolean> s) {
                mSpecimenDao.update(specimens);
            }
        };
    }
}
