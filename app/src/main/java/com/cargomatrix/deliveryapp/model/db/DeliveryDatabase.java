package com.cargomatrix.deliveryapp.model.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.cargomatrix.deliveryapp.model.data.Location;
import com.cargomatrix.deliveryapp.model.data.Specimen;

@Database(entities = { Location.class, Specimen.class}, version = DeliveryDatabase.VERSION, exportSchema = false)
public abstract class DeliveryDatabase extends RoomDatabase {

    static final int VERSION = 1;

    public abstract LocationDao getLocationDao();

    public abstract SpecimenDao getSpecimenDao();
}
