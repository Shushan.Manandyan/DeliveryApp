package com.cargomatrix.deliveryapp.model.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.cargomatrix.deliveryapp.model.data.Location;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface LocationDao {

    @Query("SELECT * FROM location")
    Flowable<List<Location>> getAll();

    @Query("SELECT COUNT(*) from location")
    Flowable<Integer> countLocations();

    @Insert
    void insertAll(Location... locations);

    @Query("DELETE FROM location where id LIKE :locationId")
    void delete(int locationId);
}
