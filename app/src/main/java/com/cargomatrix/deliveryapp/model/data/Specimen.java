package com.cargomatrix.deliveryapp.model.data;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

@Entity(tableName = "specimen")
public class Specimen {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    @ColumnInfo(name = "bar_code")
    private String mBarCode;

    @ColumnInfo(name = "name")
    private String mName;

    @ColumnInfo(name = "search_loc_id")
    private int mSearchLocationId;

    @ColumnInfo(name = "dest_loc_id")
    private int mDestinationLocationId;

    @ColumnInfo(name = "picked_up")
    private boolean mPickedUp;

    public Specimen() {
    }

    public int getId() {
        return mId;
    }

    public void setId(int uid) {
        mId = uid;
    }

    public String getBarCode() {
        return mBarCode;
    }

    public void setBarCode(String barCode) {
        mBarCode = barCode;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getSearchLocationId() {
        return mSearchLocationId;
    }

    public void setSearchLocationId(int searchLocationId) {
        mSearchLocationId = searchLocationId;
    }

    public int getDestinationLocationId() {
        return mDestinationLocationId;
    }

    public void setDestinationLocationId(int destinationLocationId) {
        mDestinationLocationId = destinationLocationId;
    }

    public boolean isPickedUp() {
        return mPickedUp;
    }

    public void setPickedUp(boolean pickedUp) {
        mPickedUp = pickedUp;
    }
}
