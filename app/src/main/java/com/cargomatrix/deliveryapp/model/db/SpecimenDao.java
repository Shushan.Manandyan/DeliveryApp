package com.cargomatrix.deliveryapp.model.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.cargomatrix.deliveryapp.model.data.Specimen;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface SpecimenDao {

    @Query("SELECT * FROM Specimen where picked_up LIKE :pickedUp")
    Flowable<List<Specimen>> getPickedUpSpecimens(boolean pickedUp);

    @Query("SELECT * FROM specimen where search_loc_id LIKE :locationId AND picked_up LIKE :pickedUp")
    Flowable<List<Specimen>> findByLocation(int locationId, boolean pickedUp);

    @Query("SELECT COUNT(*) from Specimen")
    Flowable<Integer> countItems();

    @Insert
    void insertAll(Specimen... specimens);

    @Update
    void update(Specimen specimens);
}
