package com.cargomatrix.deliveryapp.model.db;

import com.cargomatrix.deliveryapp.model.data.Specimen;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;

public interface SpecimenRepository {

    Flowable<List<Specimen>> getPickedUpSpecimens();

    Flowable<Integer> countItems();

    Flowable<List<Specimen>> findByLocation(int locationId, boolean pickedUp);

    void insertAll(Specimen... specimens);

    Flowable<Boolean> update(Specimen specimens);
}
