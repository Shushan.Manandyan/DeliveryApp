package com.cargomatrix.deliveryapp;

import android.app.Application;

import com.cargomatrix.deliveryapp.di.component.AppComponent;
import com.cargomatrix.deliveryapp.di.component.DaggerAppComponent;
import com.cargomatrix.deliveryapp.di.module.AppModule;
import com.cargomatrix.deliveryapp.di.module.RoomModule;

public class DeliveryApplication extends Application {

    private static AppComponent sComponent;

    @Override
    public void onCreate() {

        super.onCreate();
        sComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .roomModule(new RoomModule(this))
                .build();
    }

    public static AppComponent getComponent() {
        return sComponent;
    }
}
